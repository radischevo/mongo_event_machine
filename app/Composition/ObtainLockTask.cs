﻿using System;
using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public class ObtainLockTask : IApplicationTask {
		private readonly ILockManager _lockManager;

		public ObtainLockTask(ILockManager lockManager) {
			_lockManager = lockManager;
		}

		public void Execute(NameValueCollection parameters) {
			var timeout = parameters.GetAs<int>("--timeout");
			var lockKey = parameters["--key"];

			Console.WriteLine("{0}:Trying to get lock on {1}...", DateTime.UtcNow, lockKey);

			ILock cookie;
			if (_lockManager.TryAcquireLock(lockKey, TimeSpan.FromSeconds(timeout), out cookie)) {
				Console.WriteLine("{0}:The lock has been acquired!", DateTime.UtcNow);

				try {
					Console.ReadKey(true);
				}
				finally {
					_lockManager.ReleaseLock(cookie);
					Console.WriteLine("{0}:The lock has been released.", DateTime.UtcNow);
				}
			}
			else {
				Console.WriteLine("{0}:Could not acquire lock on {1}.", DateTime.UtcNow, lockKey);
			}
		}
	}
}

