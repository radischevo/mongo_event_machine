﻿using System;
using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public class EventDispatcherTask : IApplicationTask {
		private readonly IEventDispatcher _dispatcher;

		public EventDispatcherTask(IEventDispatcher dispatcher) {
			_dispatcher = dispatcher;
		}

		public void Execute(NameValueCollection parameters) {
			string str;

			while (!String.IsNullOrEmpty((str = Console.ReadLine()))) {
				_dispatcher.Dispatch(new SimpleEvent(str));
			}
		}
	}
}

