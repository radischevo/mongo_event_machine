﻿using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public interface IOptionsResolver {
		NameValueCollection ResolveOptions();
	}
}

