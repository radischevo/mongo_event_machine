﻿using System.Collections.Specialized;

using Bru.Drive.Messaging.MongoDb.Serialization;

using MongoDB.Bson.Serialization.Conventions;

namespace Bru.Drive.EventMachine.Application {
	public class InstallConventionsTask : IApplicationTask {
		public void Execute(NameValueCollection parameters) {
			var conventions = new ConventionPack {
				new MemberNameElementNameConvention(),
				new NamedParameterMapConvention(),
				new ReadOnlyPropertyFinderConvention(),
				new AlwaysIncludeTypeNameConvention(),
				new IgnoreExtraElementsConvention(true)
			};

			ConventionRegistry.Register("Apply to all", conventions, t => true);
		}
	}
}

