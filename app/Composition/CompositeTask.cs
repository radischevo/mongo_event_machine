﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public class CompositeTask : IApplicationTask {
		private readonly IEnumerable<IApplicationTask> _tasks;

		public CompositeTask(IEnumerable<IApplicationTask> tasks) {
			_tasks = tasks;
		}

		public CompositeTask(params IApplicationTask[] tasks) 
			: this((IEnumerable<IApplicationTask>)tasks) {
		}

		public void Execute(NameValueCollection parameters) {
			foreach (var task in _tasks) {
				task.Execute(parameters);
			}
		}
	}
}

