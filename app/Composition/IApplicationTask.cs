﻿using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public interface IApplicationTask {
		void Execute(NameValueCollection parameters);
	}
}

