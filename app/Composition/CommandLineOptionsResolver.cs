﻿using System;
using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	public class CommandLineOptionsResolver : IOptionsResolver {
		private static readonly char[] _separators = { '=' };

		public NameValueCollection ResolveOptions() {
			var options = new NameValueCollection();
			var args = Environment.GetCommandLineArgs();

			for (var i = 1; i < args.Length; ++i) {
				var pair = args[i].Split(_separators, StringSplitOptions.RemoveEmptyEntries);
				if (pair.Length == 1) {
					options.Add(null, pair[0]);
				}
				else if (pair.Length == 2) {
					options.Add(pair[0], pair[1]);
				}
			}

			return options;
		}
	}
}

