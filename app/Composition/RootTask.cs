﻿using System;
using System.Collections.Specialized;

using Autofac;

namespace Bru.Drive.EventMachine.Application {
	public class RootTask : IApplicationTask {
		private readonly IComponentContext _context;

		public RootTask(IComponentContext context) {
			_context = context;
		}

		public void Execute(NameValueCollection parameters) {
			var taskName = parameters.Get(null);

			if (!_context.IsRegisteredWithKey<IApplicationTask>(taskName)) {
				Console.WriteLine("Unknown task '{0}' specified.", taskName);
				return;
			}

			var task = _context.ResolveNamed<IApplicationTask>(taskName);
			task.Execute(parameters);
		}
	}
}

