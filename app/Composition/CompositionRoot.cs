﻿using System;
using System.Reflection;

using Autofac;

using Bru.Drive.Messaging;
using Bru.Drive.Messaging.MongoDb;

using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Bru.Drive.EventMachine.Application {
	public class CompositionRoot {
		public void Configure(ContainerBuilder builder) {
			builder
				.RegisterType<CommandLineOptionsResolver>()
				.As<IOptionsResolver>();

			builder
				.RegisterType<MongoClient>()
				.AsSelf()
				.WithParameter("connectionString", "mongodb://localhost:27017");

			builder
				.Register<MongoServer>(k => k.Resolve<MongoClient>().GetServer());

			builder
				.Register<MongoDatabase>(k => k.Resolve<MongoServer>().GetDatabase("event_machine"))
				.Named<MongoDatabase>("mongoDatabase.event_machine");

			builder
				.Register<MongoCollection>(k => { 
					var db = k.ResolveNamed<MongoDatabase>("mongoDatabase.event_machine");
					const int MaxSize = 128 << 8;

					if (!db.CollectionExists("events")) {
						var options = CollectionOptions.SetCapped(true).SetMaxSize(MaxSize);
						db.CreateCollection("events", options);
					}

					var settings = new MongoCollectionSettings { 
						AssignIdOnInsert = false,
						WriteConcern = WriteConcern.WMajority 
					};

					return db.GetCollection("events", settings);
				})
				.Named<MongoCollection>("mongoCollection.events");

			builder
				.Register<MongoCollection>(k => {
					var db = k.ResolveNamed<MongoDatabase>("mongoDatabase.event_machine");
					return db.GetCollection("locks", WriteConcern.WMajority);
				})
				.Named<MongoCollection>("mongoCollection.locks");

			builder
				.Register<MongoCollection>(k => {
					var db = k.ResolveNamed<MongoDatabase>("mongoDatabase.event_machine");
					if (!db.CollectionExists("counters")) {
						var options = CollectionOptions.SetAutoIndexId(false);
						db.CreateCollection("counters", options);
					}

					var settings = new MongoCollectionSettings {
						AssignIdOnInsert = false,
						WriteConcern = WriteConcern.WMajority
					};

					var collection = db.GetCollection("counters", settings);
					collection.CreateIndex(
						IndexKeys.Ascending("name", "key", "period"), 
						IndexOptions.SetUnique(true)
					);

					return collection;
				})
				.Named<MongoCollection>("mongoCollection.counters");
			
			builder
				.RegisterType<ConsoleLog>()
				.As<ILog>();

			builder
				.Register<IQueueListener<IEvent>>(k => new MongoQueueListener<IEvent>(
					k.Resolve<IOptionsResolver>().ResolveOptions().GetAs<int>("--node"),
					k.ResolveNamed<MongoCollection>("mongoCollection.events"),
					ReadPreference.SecondaryPreferred,
					TimeSpan.FromSeconds(2),
					k.Resolve<ILog>())
				);

			builder
				.Register<IQueuePublisher<IEvent>>(k => new MongoQueuePublisher<IEvent>(
					k.Resolve<IOptionsResolver>().ResolveOptions().GetAs<int>("--node"),
					k.ResolveNamed<MongoCollection>("mongoCollection.events"),
					WriteConcern.WMajority)
				);

			builder
				.RegisterType<LocalEventDispatcher>()
				.Named<IEventDispatcher>("eventDispatcher.local");

			builder
				.RegisterDecorator<IEventDispatcher>((c, inner) => 
					new QueueEventDispatcher(
						c.Resolve<IQueuePublisher<IEvent>>(),
						c.Resolve<IQueueListener<IEvent>>(),
						inner,
						c.Resolve<IDistributedEventDetector>()
					),
					"eventDispatcher.local"
				);

			builder
				.RegisterType<DistributedEventDetector>()
				.As<IDistributedEventDetector>();

			builder
				.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
				.AsClosedTypesOf(typeof(IEventHandler<>))
				.Except<LockAcquiredEventHandler>(cr => cr
					.WithParameter("acquired", c => c.ResolveNamed<IKeyedCounter<string>>("counter.acquired_locks"))
					.WithParameter("timedOut", c => c.ResolveNamed<IKeyedCounter<string>>("counter.timed_out_locks"))
					.AsImplementedInterfaces()
				);

			builder
				.Register<ILockManager>(k => new MongoLockManager(
					k.ResolveNamed<MongoCollection>("mongoCollection.locks"),
					TimeSpan.FromHours(1),
					WriteConcern.WMajority)
				)
				.Named<ILockManager>("lockManager.mongo");

			builder
				.RegisterDecorator<ILockManager>((c, inner) => 
					new EventSourcingLockManager(inner, c.Resolve<IEventDispatcher>()), 
					"lockManager.mongo"
				);

			builder
				.Register(c => new NormalizingTimeAdjuster(TimeSpan.FromSeconds(10)))
				.Named<ITimeAdjuster>("timeAdjuster.10_sec");

			builder
				.Register(c => new NamedMongoCounter<CounterName, string>(
					CounterName.AcquiredLocks, 
					c.ResolveNamed<MongoCollection>("mongoCollection.counters"),
					WriteConcern.WMajority,
					c.ResolveNamed<ITimeAdjuster>("timeAdjuster.10_sec"))
				)
				.Named<IKeyedCounter<string>>("counter.acquired_locks");

			builder
				.Register(c => new NamedMongoCounter<CounterName, string>(
					CounterName.TimedOutLocks, 
					c.ResolveNamed<MongoCollection>("mongoCollection.counters"),
					WriteConcern.WMajority,
					c.ResolveNamed<ITimeAdjuster>("timeAdjuster.10_sec"))
				)
				.Named<IKeyedCounter<string>>("counter.timed_out_locks");

			builder
				.RegisterType<EventDispatcherTask>()
				.Named<IApplicationTask>("events");

			builder
				.RegisterType<ObtainLockTask>()
				.Named<IApplicationTask>("lock");

			builder.RegisterType<RootTask>().AsSelf();
			builder.RegisterType<InstallConventionsTask>().AsSelf();

			builder
				.Register(c => new CompositeTask(
					c.Resolve<InstallConventionsTask>(), 
					c.Resolve<RootTask>())
				)
				.As<IApplicationTask>();
		}
	}
}

