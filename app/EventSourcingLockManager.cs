﻿using System;

namespace Bru.Drive.EventMachine.Application {
	public class EventSourcingLockManager : ILockManager {
		private readonly ILockManager _underlying;
		private readonly IEventDispatcher _eventDispatcher;

		public EventSourcingLockManager(ILockManager underlying, IEventDispatcher eventDispatcher) {
			_underlying = underlying;
			_eventDispatcher = eventDispatcher;
		}

		public ILock AcquireLock(string key, TimeSpan? timeout) {
			var cookie = _underlying.AcquireLock(key, timeout);
			_eventDispatcher.Dispatch(new LockAcquiredEvent(key));

			return cookie;
		}

		public bool TryAcquireLock(string key, TimeSpan? timeout, out ILock cookie) {
			if (_underlying.TryAcquireLock(key, timeout, out cookie)) {
				_eventDispatcher.Dispatch(new LockAcquiredEvent(key));
				return true;
			}

			_eventDispatcher.Dispatch(new LockTimedOutEvent(key));
			return false;
		}

		public void ReleaseLock(ILock cookie) {
			_underlying.ReleaseLock(cookie);
			_eventDispatcher.Dispatch(new LockReleasedEvent(cookie.Key));
		}
	}
}

