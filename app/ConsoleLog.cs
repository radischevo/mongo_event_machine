﻿using System;

using Bru.Drive.Messaging.MongoDb;

namespace Bru.Drive.EventMachine.Application {
	public class ConsoleLog : ILog {
		public void Info(string message, params object[] args) {
			Console.WriteLine(message, args);
		}

		public void Warn(string message, params object[] args) {
			Console.WriteLine(message, args);
		}

		public void Error(string message, Exception error) {
			if (error == null) {
				Console.WriteLine(message);
			}
			else {
				Console.WriteLine(message + error);
			}
		}
	}
}

