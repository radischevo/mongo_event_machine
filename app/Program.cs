﻿using Autofac;

namespace Bru.Drive.EventMachine.Application {
	public class Program {
		public static void Main(string[] args) {
			var builder = new ContainerBuilder();
			var composer = new CompositionRoot();

			composer.Configure(builder);

			using (var container = builder.Build()) {
				using (container.BeginLifetimeScope()) {
					var resolver = container.Resolve<IOptionsResolver>();
					var task = container.Resolve<IApplicationTask>();

					task.Execute(resolver.ResolveOptions());
				}
			}
		}
	}
}
