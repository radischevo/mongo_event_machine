﻿using System;

namespace Bru.Drive.EventMachine.Application {
	public class LockAcquiredEventHandler : IEventHandler<LockAcquiredEvent>, IEventHandler<LockTimedOutEvent> {
		private readonly IKeyedCounter<string> _acquired;
		private readonly IKeyedCounter<string> _timedOut;

		public LockAcquiredEventHandler(IKeyedCounter<string> acquired, IKeyedCounter<string> timedOut) {
			_acquired = acquired;
			_timedOut = timedOut;
		}

		public void Handle(LockAcquiredEvent evt) {
			_acquired.Increment(evt.Key);

			Console.WriteLine("=> Lock on '{0}' has been acquired...", evt.Key);
		}

		public void Handle(LockTimedOutEvent evt) {
			_timedOut.Increment(evt.Key);

			Console.WriteLine("=> Lock on '{0}' has been released.", evt.Key);
		}
	}
}

