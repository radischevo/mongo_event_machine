﻿using System;

namespace Bru.Drive.EventMachine.Application {
	public class SimpleEventHandler : IEventHandler<SimpleEvent> {
		public void Handle(SimpleEvent evt) {
			Console.WriteLine("=> {0}", evt.Message);
		}
	}
}

