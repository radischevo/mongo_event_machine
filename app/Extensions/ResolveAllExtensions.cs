﻿using System;
using System.Collections.Generic;
using System.Linq;

using Autofac;
using Autofac.Core;

namespace Bru.Drive.EventMachine.Application {
	public static class ResolveAllExtensions {
		public static T[] ResolveAll<T>(this IComponentContext context) {
			return context.Resolve<IEnumerable<T>>().ToArray();
		}

		public static object[] ResolveAll(this IComponentContext context, Type type) {
			var typedEnumerable = typeof(IEnumerable<>).MakeGenericType(type);
			return (object[])context.ResolveService(new TypedService(typedEnumerable));
		}
	}
}

