﻿using System;
using System.Collections.Specialized;

namespace Bru.Drive.EventMachine.Application {
	internal static class NameValueCollectionExtensions {
		public static T GetAs<T>(this NameValueCollection collection, string name) {
			var value = collection.Get(name);
			return (T)Convert.ChangeType(value, typeof(T));
		}
	}
}

