﻿using System;

using Autofac;
using Autofac.Builder;
using Autofac.Core;

namespace Bru.Drive.EventMachine.Application {
	public static class ParameterRegistrationExtensions {
		public static IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> WithParameter<TLimit, TReflectionActivatorData, TStyle, TParameter>(
			this IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> registration, 
			Func<IComponentContext, TParameter> resolver) 
			where TReflectionActivatorData : ReflectionActivatorData {
			var parameter = new ResolvedParameter((p, c) => p.ParameterType == typeof(TParameter), (p, c) => resolver(c));
			return registration.WithParameter(parameter);
		}

		public static IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> WithParameter<TLimit, TReflectionActivatorData, TStyle>(
			this IRegistrationBuilder<TLimit, TReflectionActivatorData, TStyle> registration, 
			string parameterName,
			Func<IComponentContext, object> resolver) 
			where TReflectionActivatorData : ReflectionActivatorData {
			var parameter = new ResolvedParameter((p, c) => p.Name == parameterName, (p, c) => resolver(c));
			return registration.WithParameter(parameter);
		}
	}
}

