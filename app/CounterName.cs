﻿namespace Bru.Drive.EventMachine.Application {
	public enum CounterName {
		AcquiredLocks = 0,
		TimedOutLocks = 1
	}
}

