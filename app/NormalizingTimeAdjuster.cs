﻿using System;
using System.Diagnostics.Contracts;

using Bru.Drive.Messaging.MongoDb;

namespace Bru.Drive.EventMachine.Application {
	public class NormalizingTimeAdjuster : ITimeAdjuster {
		private readonly TimeSpan _unit;

		public NormalizingTimeAdjuster(TimeSpan unit) {
			Contract.Requires(unit > TimeSpan.Zero);

			_unit = unit;
		}

		public DateTime Adjust(DateTime timestamp) {
			return new DateTime((long)Math.Round((double)timestamp.Ticks / _unit.Ticks) * _unit.Ticks, timestamp.Kind);
		}
	}
}

