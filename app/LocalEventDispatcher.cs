﻿using System;
using System.Collections.Concurrent;
using System.Linq.Expressions;
using System.Reflection;

using Autofac;

namespace Bru.Drive.EventMachine.Application {
	public class LocalEventDispatcher : IEventDispatcher {
		private static readonly ConcurrentDictionary<Type, Action<object, IEvent>> _methodCache
			= new ConcurrentDictionary<Type, Action<object, IEvent>>();

		private readonly IComponentContext _context;

		public LocalEventDispatcher(IComponentContext context) {
			_context = context;
		}

		public void Dispatch<TEvent>(TEvent evt) 
			where TEvent : class, IEvent {
			var eventType = evt.GetType();
			var handlerType = GetHandlerType(eventType);

			var handlers = _context.ResolveAll(handlerType);
			var method = _methodCache.GetOrAdd(eventType, BuildHandlerMethod);

			foreach (var handler in handlers) {
				method(handler, evt);
			}
		}

		private static Action<object, IEvent> BuildHandlerMethod(Type eventType) {
			var handlerType = GetHandlerType(eventType);
			var handleMethod = handlerType.GetMethod("Handle", BindingFlags.Public | BindingFlags.Instance);

			var handlerParameter = Expression.Parameter(typeof(object), "handler");
			var typedHandlerParameter = Expression.Convert(handlerParameter, handlerType);

			var eventParameter = Expression.Parameter(typeof(IEvent), "evt");
			var typedEventParameter = Expression.Convert(eventParameter, eventType);

			var handleExpression = Expression.Call(typedHandlerParameter, handleMethod, typedEventParameter);

			var lambda = Expression.Lambda<Action<object, IEvent>>(handleExpression, handlerParameter, eventParameter);

			return lambda.Compile();
		}

		private static Type GetHandlerType(Type eventType) {
			return typeof(IEventHandler<>).MakeGenericType(eventType);
		}
	}
}

