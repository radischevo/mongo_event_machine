namespace Bru.Drive.EventMachine.Application {
	public class SimpleEvent : IEvent {
		private readonly string _message;

		public SimpleEvent(string message) {
			_message = message;
		}

		public string Message {
			get {
				return _message;
			}
		}

		public override string ToString() {
			return string.Format("[SimpleEvent: Message={0}]", _message);
		}
	}
}
