﻿using System;

namespace Bru.Drive.EventMachine.Application {
	public class LockTimedOutEvent : IEvent {
		private readonly string _key;

		public LockTimedOutEvent(string key) {
			_key = key;
		}

		public string Key {
			get {
				return _key;
			}
		}
	}
}

