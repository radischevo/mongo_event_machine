﻿namespace Bru.Drive.EventMachine.Application {
	public class LockAcquiredEvent : IEvent {
		private readonly string _key;

		public LockAcquiredEvent(string key) {
			_key = key;
		}

		public string Key {
			get {
				return _key;
			}
		}
	}
}

