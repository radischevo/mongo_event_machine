﻿namespace Bru.Drive.EventMachine.Application {
	public class LockReleasedEvent : IEvent {
		private readonly string _key;

		public LockReleasedEvent(string key) {
			_key = key;
		}

		public string Key {
			get {
				return _key;
			}
		}
	}
}

