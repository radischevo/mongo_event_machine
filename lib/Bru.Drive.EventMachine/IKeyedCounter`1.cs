﻿namespace Bru.Drive.EventMachine {
	public interface IKeyedCounter<TKey> {
		int GetValue(TKey key);

		void Increment(TKey key);
	}
}

