﻿using System;

namespace Bru.Drive.EventMachine {
	/// <summary>
	/// Describes a handle to the resource lock.
	/// </summary>
	public interface ILock {
		/// <summary>
		/// Gets the locking key.
		/// </summary>
		string Key { 
			get; 
		}

		/// <summary>
		/// Gets the lock validity period.
		/// </summary>
		TimeSpan Validity {
			get;
		}
	}
}

