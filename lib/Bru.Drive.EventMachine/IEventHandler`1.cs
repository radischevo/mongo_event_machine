﻿using System;

namespace Bru.Drive.EventMachine {
	/// <summary>
	/// Introduces a contract for handling particular events.
	/// </summary>
	public interface IEventHandler<in TEvent>
		where TEvent : IEvent {
		/// <summary>
		/// Handles the specified event.
		/// </summary>
		/// <param name="evt">The event instance to handle.</param>
		void Handle(TEvent evt);
	}
}

