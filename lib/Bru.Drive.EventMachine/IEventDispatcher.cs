﻿namespace Bru.Drive.EventMachine {
	/// <summary>
	/// Introduces a contract for the event dispatching service.
	/// </summary>
	public interface IEventDispatcher {
		/// <summary>
		/// Dispatches the specified event.
		/// </summary>
		/// <param name="evt">The event object to dispatch</param>
		/// <typeparam name="TEvent">The type of the event.</typeparam>
		void Dispatch<TEvent>(TEvent evt) 
			where TEvent : class, IEvent;
	}
}

