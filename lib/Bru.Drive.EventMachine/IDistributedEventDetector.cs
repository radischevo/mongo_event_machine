﻿namespace Bru.Drive.EventMachine {
	public interface IDistributedEventDetector {
		bool IsDistributed(IEvent evt);
	}
}

