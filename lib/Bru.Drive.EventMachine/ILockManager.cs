﻿using System;

namespace Bru.Drive.EventMachine {
	public interface ILockManager {
		ILock AcquireLock(string key, TimeSpan? timeout);

		bool TryAcquireLock(string key, TimeSpan? timeout, out ILock cookie);

		void ReleaseLock(ILock cookie);
	}
}

