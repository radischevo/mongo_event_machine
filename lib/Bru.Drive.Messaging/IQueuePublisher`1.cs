﻿namespace Bru.Drive.Messaging {
	/// <summary>
	/// Introduces a contract for a message publisher.
	/// </summary>
	public interface IQueuePublisher<in TMessage> {
		/// <summary>
		/// Publishes the specified message into a particular queue.
		/// </summary>
		/// <param name="message">The message to publish.</param>
		void Publish(TMessage message);
	}
}

