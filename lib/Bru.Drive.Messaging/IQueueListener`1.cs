﻿using System;

namespace Bru.Drive.Messaging {
	public interface IQueueListener<out TMessage> {
		event Action<TMessage> MessageReceived;
	}
}

