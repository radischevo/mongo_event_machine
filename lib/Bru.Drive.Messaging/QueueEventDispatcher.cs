﻿using System;

using Bru.Drive.EventMachine;

namespace Bru.Drive.Messaging {
	public class QueueEventDispatcher : IEventDispatcher, IDisposable {
		private readonly IQueuePublisher<IEvent> _queuePublisher;
		private readonly IQueueListener<IEvent> _queueListener;
		private readonly IEventDispatcher _localEventDispatcher;
		private readonly IDistributedEventDetector _distributedEventDetector;

		public QueueEventDispatcher(
			IQueuePublisher<IEvent> queuePublisher,
			IQueueListener<IEvent> queueListener,
			IEventDispatcher localEventDispatcher,
			IDistributedEventDetector distributedEventDetector) {
			_queuePublisher = queuePublisher;
			_queueListener = queueListener;
			_localEventDispatcher = localEventDispatcher;
			_distributedEventDetector = distributedEventDetector;

			_queueListener.MessageReceived += OnRemoteEventReceived;
		}

		public void Dispatch<TEvent>(TEvent evt) 
			where TEvent : class, IEvent {
			if (_distributedEventDetector.IsDistributed(evt)) {
				_queuePublisher.Publish(evt);
			}

			_localEventDispatcher.Dispatch(evt);
		}

		private void OnRemoteEventReceived(IEvent remoteEvent) {
			_localEventDispatcher.Dispatch(remoteEvent);
		}

		public void Dispose() {
			_queueListener.MessageReceived -= OnRemoteEventReceived;
		}
	}
}

