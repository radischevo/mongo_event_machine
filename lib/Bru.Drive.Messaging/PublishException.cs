﻿using System;

namespace Bru.Drive.Messaging {
	public class PublishException : QueueException {
		public PublishException(Exception innerException)
			: this(innerException.Message, innerException) {
		}

		public PublishException(string message, Exception innerException)
			: base(message, innerException) {
		}
	}
}

