﻿using System;

namespace Bru.Drive.Messaging {
	public class QueueException : ApplicationException {
		public QueueException(Exception innerException)
			: this(innerException.Message, innerException) {
		}

		public QueueException(string message, Exception innerException)
			: base(message, innerException) {
		}
	}
}