﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

using Bru.Drive.EventMachine;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Bru.Drive.Messaging.MongoDb {
	public class MongoLockManager : ILockManager {
		private const int _pollInterval = 100;

		private readonly MongoCollection _collection;
		private readonly TimeSpan _lifetime;
		private readonly WriteConcern _writeConcern;

		public MongoLockManager(MongoCollection collection, TimeSpan lifetime, WriteConcern writeConcern) {
			Contract.Requires(collection != null);
			Contract.Requires(writeConcern != null);

			_collection = collection;
			_lifetime = lifetime;
			_writeConcern = writeConcern;
		}

		public ILock AcquireLock(string key, TimeSpan? timeout) {
			ILock cookie;

			if (!TryAcquireLock(key, timeout, out cookie)) {
				throw new TimeoutException(String.Format("Timeout of {0} exceeded.", timeout));
			}

			return cookie;
		}

		public bool TryAcquireLock(string key, TimeSpan? timeout, out ILock cookie) {
			var cts = new CancellationTokenSource();
			var actualTimeout = timeout ?? Timeout.InfiniteTimeSpan;

			cookie = null;

			var task = Task.Factory.StartNew(state => {
				var token = (CancellationToken)state;
				ILock result;

				while (!token.IsCancellationRequested) {
					result = AcquireLockInternal(key, timeout);
					if (!ReferenceEquals(result, null)) {
						return result;
					}

					Thread.Sleep(_pollInterval);
				}

				return null;
			}, cts.Token, cts.Token);

			if (!task.Wait(actualTimeout)) {
				cts.Cancel();
				return false;
			}

			cookie = task.Result;
			return true;
		}

		private ILock AcquireLockInternal(string key, TimeSpan? timeout) {
			var validity = timeout ?? _lifetime;
			var expiresAt = GetExpiration(validity);
			var version = ThreadSafeRandom.Next();

			var entry = new BsonDocument {
				{ "_id", key },
				{ "expires_at", expiresAt },
				{ "version", version }
			};

			var existingEntry = _collection.FindOneByIdAs<BsonDocument>(key);
			if (ReferenceEquals(existingEntry, null)) {
				try {
					_collection.Insert(entry, _writeConcern);
					return new MongoLock(key, validity, version);
				}
				catch (MongoDuplicateKeyException) {
					// We are late for the dinner
					return null;
				}
				catch (MongoWriteConcernException) {
					return null;
				}
			}

			// If we got here then a key for the lock is present. This could be because the lock is
			// correctly acquired or it could be because a client that had acquired the lock crashed (or didn't release it properly).
			// Therefore we need to get the value of the lock to see when it should expire
			var lockedUntil = existingEntry.GetValue("expires_at").AsInt64;

			// If the expire time is greater than the current time then we can't let the lock go yet
			if (lockedUntil > DateTime.UtcNow.ToUnixTimeMs()) {
				return null;
			}

			// If the expire time is less than the current time then it wasn't released properly and we can attempt to 
			// acquire the lock. This is done by setting the lock to our timeout and version.
			var result = _collection.Update(
				Query.And(
					Query.EQ("_id", key), 
					Query.EQ("version", existingEntry.GetValue("version").AsInt32)
				),
				Update
					.Set("expires_at", GetExpiration(validity))
					.Set("version", version)
			);

			if (result.DocumentsAffected == 1) {
				return new MongoLock(key, validity, version);
			}

			return null;
		}

		public void ReleaseLock(ILock cookie) {
			var typedCookie = (MongoLock)cookie;
			var query = Query.And(
	            Query.EQ("_id", typedCookie.Key),
	            Query.EQ("version", typedCookie.Version)
            );

			_collection.Remove(query, _writeConcern);
		}

		private static long GetExpiration(TimeSpan timeout) {
			return DateTime.UtcNow.Add(timeout).ToUnixTimeMs(); 
		}
	}
}

