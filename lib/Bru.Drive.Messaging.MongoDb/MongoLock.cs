﻿using System;
using System.Diagnostics.Contracts;

using Bru.Drive.EventMachine;

namespace Bru.Drive.Messaging.MongoDb {
	internal sealed class MongoLock : ILock {
		private readonly string _key;
		private readonly TimeSpan _validity;
		private readonly int _version;

		public MongoLock(string key, TimeSpan validity, int version) {
			Contract.Requires(!String.IsNullOrEmpty(key));

			_key = key;
			_validity = validity;
			_version = version;
		}

		public string Key {
			get {
				return _key;
			}
		}

		public TimeSpan Validity {
			get {
				return _validity;
			}
		}

		public int Version {
			get {
				return _version;
			}
		}
	}
}

