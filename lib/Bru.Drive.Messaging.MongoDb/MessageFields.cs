﻿namespace Bru.Drive.Messaging.MongoDb {
	internal static class MessageFields {
		public const string Id = "_id";
		public const string IssuerId = "issuerId";
		public const string Message = "message";
	}
}

