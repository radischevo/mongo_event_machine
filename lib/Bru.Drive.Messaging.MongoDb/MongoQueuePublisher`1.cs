﻿using System.Diagnostics.Contracts;

using MongoDB.Driver;
using MongoDB.Bson;

namespace Bru.Drive.Messaging.MongoDb {
	public class MongoQueuePublisher<TMessage> : IQueuePublisher<TMessage> {
		private readonly int _nodeId;
		private readonly MongoCollection _collection;
		private readonly WriteConcern _writeConcern;

		public MongoQueuePublisher(
			int nodeId, 
			MongoCollection collection, 
			WriteConcern writeConcern) {
			Contract.Requires(collection != null);
			Contract.Requires(writeConcern != null);

			_nodeId = nodeId;
			_collection = collection;
			_writeConcern = writeConcern;
		}

		public void Publish(TMessage message) {
			var entry = new BsonDocument {
				{ MessageFields.IssuerId, _nodeId },
				{ MessageFields.Message, message.ToBsonDocument<TMessage>() }
			};

			_collection.Insert(entry, _writeConcern);
		}
	}
}

