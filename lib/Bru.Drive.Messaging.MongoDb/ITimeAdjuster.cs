﻿using System;

namespace Bru.Drive.Messaging.MongoDb {
	public interface ITimeAdjuster {
		DateTime Adjust(DateTime timestamp);
	}
}

