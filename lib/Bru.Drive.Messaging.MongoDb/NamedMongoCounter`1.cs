﻿using System;
using System.Diagnostics.Contracts;

using Bru.Drive.EventMachine;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Bru.Drive.Messaging.MongoDb {
	public class NamedMongoCounter<TName, TKey> : IKeyedCounter<TKey> {
		private readonly TName _name;
		private readonly MongoCollection _collection;
		private readonly WriteConcern _writeConcern;
		private readonly ITimeAdjuster _timeAdjuster;

		public NamedMongoCounter(
			TName name,
			MongoCollection collection, 
			WriteConcern writeConcern, 
			ITimeAdjuster timeAdjuster) {
			Contract.Requires(!ReferenceEquals(name, null));
			Contract.Requires(collection != null);
			Contract.Requires(writeConcern != null);
			Contract.Requires(timeAdjuster != null);

			_name = name;
			_collection = collection;
			_writeConcern = writeConcern;
			_timeAdjuster = timeAdjuster;
		}

		public void Increment(TKey key) {
			var timestamp = _timeAdjuster.Adjust(DateTime.UtcNow);

			_collection.Update(
				Query.And(
					Query.EQ("name", BsonValue.Create(_name)),
					Query.EQ("key", BsonValue.Create(key)),
					Query.EQ("period", timestamp.ToUnixTimeMs())
				),
				Update.Inc("value", 1),
				new MongoUpdateOptions {
					Flags = UpdateFlags.Upsert,
					WriteConcern = _writeConcern
				}
			);
		}

		public int GetValue(TKey key) {
			var timestamp = _timeAdjuster.Adjust(DateTime.UtcNow);
			var counter = _collection.FindOneAs<BsonDocument>(
				new FindOneArgs { 
					Query = Query.And(
						Query.EQ("name", BsonValue.Create(_name)),
						Query.EQ("key", BsonValue.Create(key)), 
						Query.EQ("period", timestamp.ToUnixTimeMs())
					),
					Fields = Fields.Include("value")
				}
			);

			return (counter == null) ? 0 : counter["value"].AsInt32;
		}
	}
}

