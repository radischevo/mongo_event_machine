﻿using System;
using System.Security.Cryptography;

namespace Bru.Drive.Messaging.MongoDb {
	internal static class ThreadSafeRandom {
		private static readonly RNGCryptoServiceProvider _rng = new RNGCryptoServiceProvider();
		[ThreadStatic]
		private static Random _local;

		public static int Next() {
			return Local.Next();
		}

		public static double NextDouble() {
			return Local.NextDouble();
		}

		public static void NextBytes(byte[] buffer) {
			Local.NextBytes(buffer);
		}

		private static Random Local {
			get {
				var instance = _local;
				if (instance == null) {
					var buffer = new byte[4];
					_rng.GetBytes(buffer);
					_local = instance = new Random(BitConverter.ToInt32(buffer, 0));
				}

				return instance;
			}
		}
	}
}

