﻿using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Bru.Drive.Messaging.MongoDb {
	public class MongoQueueListener<TMessage> : IQueueListener<TMessage>, IDisposable {
		private readonly object _subscriptionLock = new object();
		private readonly CancellationTokenSource _cancellationTokenSource;

		private readonly int _nodeId;
		private readonly MongoCollection _collection;
		private readonly ReadPreference _readPreference;
		private readonly TimeSpan _pollInterval;
		private readonly ILog _log;

		private event Action<TMessage> _messageReceived;

		private Task _pollingTask;

		public MongoQueueListener(
			int nodeId, 
			MongoCollection collection, 
			ReadPreference readPreference,
			TimeSpan pollInterval,
			ILog log) {
			Contract.Requires(collection != null);
			Contract.Requires(readPreference != null);
			Contract.Requires(pollInterval > TimeSpan.Zero);
			Contract.Requires(log != null);

			_nodeId = nodeId;
			_collection = collection;
			_readPreference = readPreference;
			_pollInterval = pollInterval;
			_log = log;

			_cancellationTokenSource = new CancellationTokenSource();
		}

		public event Action<TMessage> MessageReceived {
			add {
				_messageReceived += value;
				EnsureSubscribed();
			}
			remove {
				_messageReceived -= value;
			}
		}

		private void OnMessageReceived(TMessage message) {
			var handler = _messageReceived;
			if (handler != null)
				handler(message);
		}

		private void EnsureSubscribed() {
			if (_pollingTask == null) {
				lock (_subscriptionLock) {
					if (_pollingTask == null) {
						_log.Info("Subscribed to the queue with nodeId={0}", _nodeId);

						var token = _cancellationTokenSource.Token;

						_pollingTask = new Task(() => PollForChanges(token), token, TaskCreationOptions.LongRunning);
						_pollingTask.ContinueWith(
							task => _log.Error("Exception in the queue listener task.", task.Exception), 
							TaskContinuationOptions.OnlyOnFaulted
						);

						_pollingTask.Start();
					}
				}
			}
		}

		private void PollForChanges(CancellationToken token) {
			var baseWait = (int)_pollInterval.TotalMilliseconds;
			var maxWait = baseWait * 10;

			var startingTime = DateTime.UtcNow;
			var wait = baseWait;

			while (!token.IsCancellationRequested) {
				var query = Query.GTE(MessageFields.Id, new ObjectId(startingTime, 0, 0, 0));
				var cursor = _collection.FindAs<BsonDocument>(query)
					.SetSortOrder(SortBy.Ascending("$natural"))
					.SetFlags(QueryFlags.TailableCursor | QueryFlags.NoCursorTimeout)
					.SetReadPreference(_readPreference);

				using (var enumerator = new MongoCursorEnumerator<BsonDocument>(cursor)) {
					var hasData = false;
					while (true) {
						if (token.IsCancellationRequested)
							break;

						if (enumerator.MoveNext()) {
							hasData = true;
							wait = baseWait;

							var element = enumerator.Current;

							var createdAt = element[MessageFields.Id].AsObjectId.CreationTime;
							if (startingTime < createdAt)
								startingTime = createdAt;

							if (element[MessageFields.IssuerId] == _nodeId)
								continue;

							TMessage message;
							if (TryDeserialize(element[MessageFields.Message].AsBsonDocument, out message)) {
								OnMessageReceived(message);
							}
						}
						else {
							SafeDelay(wait, token);
							if (!hasData)
								wait = Math.Min(wait * 2, maxWait);

							if (enumerator.IsDead)
								break;
						}
					}
				}
			}
		}

		private static void SafeDelay(int timeout, CancellationToken token) {
			try {
				Task.Delay(timeout, token).Wait();
			}
			catch (AggregateException e) {
				if (!(e.InnerException is TaskCanceledException))
					throw;
			}
		}

		private static bool TryDeserialize(BsonDocument document, out TMessage message) {
			message = default(TMessage);

			try {
				message = BsonSerializer.Deserialize<TMessage>(document);
				return true;
			}
			catch (BsonSerializationException) {
				return false;
			}
		}

		public void Dispose() {
			if (!_cancellationTokenSource.IsCancellationRequested) {
				_cancellationTokenSource.Cancel();
				_pollingTask.Wait();
			}

			_cancellationTokenSource.Dispose();
		}
	}
}

