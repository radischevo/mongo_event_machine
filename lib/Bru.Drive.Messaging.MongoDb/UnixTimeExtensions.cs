﻿using System;

namespace Bru.Drive.Messaging.MongoDb {
	internal static class UnixTimeExtensions {
		private static readonly DateTime _unixEpochUtc = new DateTime(621355968000000000L, DateTimeKind.Utc);
		private static readonly DateTime _minDateTimeUtc = new DateTime(1, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public static long ToUnixTimeMs(this DateTime value) {
			return (long)(value - _unixEpochUtc).TotalMilliseconds;
		}

		private static TimeSpan TimeSpanSinceUnixEpoch(DateTime dateTime) {
			var utcDatetime = dateTime;
			var timeZoneInfo = GetLocalTimeZoneInfo();

			if (dateTime.Kind != DateTimeKind.Utc) {
				utcDatetime = dateTime.Kind == DateTimeKind.Unspecified && dateTime > DateTime.MinValue
					? DateTime.SpecifyKind(dateTime.Subtract(timeZoneInfo.GetUtcOffset(dateTime)), DateTimeKind.Utc)
					: ToStableUniversalTime(dateTime);
			}

			return utcDatetime.Subtract(_unixEpochUtc);
		}

		private static DateTime ToStableUniversalTime(DateTime dateTime) {
			if (dateTime.Kind == DateTimeKind.Utc) {
				return dateTime;
			}

			if (dateTime == DateTime.MinValue) {
				return _minDateTimeUtc;
			}

			return dateTime.ToUniversalTime();
		}

		private static TimeZoneInfo GetLocalTimeZoneInfo() {
			try {
				return TimeZoneInfo.Local;
			}
			catch (Exception) {
				return TimeZoneInfo.Utc;
			}
		}
	}
}

