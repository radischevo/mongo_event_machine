﻿using System;
using System.Collections.Generic;
using System.Reflection;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.Conventions;

namespace Bru.Drive.Messaging.MongoDb.Serialization {
	public class NamedParameterMapConvention : ConventionBase, IClassMapConvention {
		private readonly BindingFlags _bindingFlags;

		public NamedParameterMapConvention()
			: this(BindingFlags.Instance | BindingFlags.Public) {
		}

		public NamedParameterMapConvention(BindingFlags bindingFlags) {
			_bindingFlags = bindingFlags;
		}

		public void Apply(BsonClassMap classMap) {
			if (ReferenceEquals(classMap, null)) {
				throw new ArgumentNullException("classMap");
			}

			if (classMap.HasCreatorMaps) {
				return;
			}

			foreach (var constructor in classMap.ClassType.GetConstructors(_bindingFlags)) {
				MapConstructor(constructor, classMap);
			}
		}

		private void MapConstructor(ConstructorInfo constructor, BsonClassMap classMap) {
			if (constructor.GetCustomAttribute<BsonConstructorAttribute>() != null) {
				return; // Conventions specified by the attribute.
			}

			var parameters = constructor.GetParameters();
			var arguments = new List<MemberInfo>();

			var creatorMap = classMap.MapConstructor(constructor);

			foreach (var parameter in parameters) {
				var argument = FindMatchingArgument(classMap.ClassType, parameter);
				if (argument == null) {
					var message = string.Format("Unable to find a matching member to provide the value for parameter '{0}'.", parameter.Name);
					throw new BsonException(message);
				}

				arguments.Add(argument);
			}

			creatorMap.SetArguments(arguments);
		}

		private MemberInfo FindMatchingArgument(Type classType, ParameterInfo parameter) {
			MemberInfo argument;
			if ((argument = Match(classType, MemberTypes.Property, BindingFlags.Public, parameter)) != null) {
				return argument;
			}

			if ((argument = Match(classType, MemberTypes.Field, BindingFlags.Public, parameter)) != null) {
				return argument;
			}

			if ((argument = Match(classType, MemberTypes.Property, BindingFlags.NonPublic, parameter)) != null) {
				return argument;
			}

			if ((argument = Match(classType, MemberTypes.Field, BindingFlags.NonPublic, parameter)) != null) {
				return argument;
			}

			return null;
		}

		private Type GetMemberType(MemberInfo memberInfo) {
			var fieldInfo = memberInfo as FieldInfo;
			if (fieldInfo != null) {
				return fieldInfo.FieldType;
			}

			var propertyInfo = memberInfo as PropertyInfo;
			if (propertyInfo != null) {
				return propertyInfo.PropertyType;
			}

			throw new BsonInternalException();
		}

		private MemberInfo Match(Type classType, MemberTypes memberType, BindingFlags visibility, ParameterInfo parameter) {
			var bindingAttr = BindingFlags.IgnoreCase | BindingFlags.Instance;
			var memberInfos = classType.GetMember(parameter.Name, memberType, bindingAttr | visibility);
			if (memberInfos.Length == 1 && GetMemberType(memberInfos[0]) == parameter.ParameterType) {
				return memberInfos[0];
			}

			return null;
		}
	}
}

