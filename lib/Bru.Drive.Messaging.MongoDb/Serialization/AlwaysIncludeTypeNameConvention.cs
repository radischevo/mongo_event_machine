﻿using System;

using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace Bru.Drive.Messaging.MongoDb.Serialization {
	public class AlwaysIncludeTypeNameConvention : ConventionBase, IClassMapConvention {
		public void Apply(BsonClassMap classMap) {
			classMap.SetDiscriminatorIsRequired(true);
			classMap.SetDiscriminator(GetWeakQualifiedName(classMap.ClassType));
		}

		private static string GetWeakQualifiedName(Type type) {
			return type.FullName + ", " + type.Assembly.GetName().Name;
		}
	}
}

