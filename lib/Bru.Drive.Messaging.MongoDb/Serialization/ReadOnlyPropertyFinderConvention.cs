﻿using System;
using System.Reflection;

using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace Bru.Drive.Messaging.MongoDb.Serialization {
	public class ReadOnlyPropertyFinderConvention : ConventionBase, IClassMapConvention {
		private readonly BindingFlags _bindingFlags;

		public ReadOnlyPropertyFinderConvention()
			: this(BindingFlags.Instance | BindingFlags.Public) {
		}

		public ReadOnlyPropertyFinderConvention(BindingFlags bindingFlags) {
			_bindingFlags = bindingFlags;
		}

		public void Apply(BsonClassMap classMap) {
			if (ReferenceEquals(classMap, null)) {
				throw new ArgumentNullException("classMap");
			}

			var properties = classMap.ClassType.GetProperties(_bindingFlags);
			foreach (var property in properties) {
				if (property.CanRead && property.DeclaringType == classMap.ClassType) {
					classMap.MapMember(property);
				}
			}
		}
	}
}

