﻿using System;

namespace Bru.Drive.Messaging.MongoDb {
	public interface ILog {
		void Info(string message, params object[] args);

		void Warn(string message, params object[] args);

		void Error(string message, Exception error);
	}
}

